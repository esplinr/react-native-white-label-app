# React Native MSDK SDK Known Issues

The following are known issues React Native MSDK that customers may run into:

* Push Notifications may not be received by the app sometimes.
